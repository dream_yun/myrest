package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"strconv"
)

var Version = "No version provided"

func Ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong")
}

func Divide(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	a := vars["a"]
	b := vars["b"]

	ai, _ := strconv.Atoi(a)
	bi, _ := strconv.Atoi(b)

	fmt.Fprintf(w, "%d", ai/bi)
}

func main() {
	ver := flag.Bool("v", false, "Print version")
	flag.Parse()

	if *ver {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		os.Exit(0)
	}
	router := mux.NewRouter()

	router.HandleFunc("/ping", Divide).Methods("GET")
	router.HandleFunc("/divide/{a}/{b}", Divide).Methods("GET")

	http.Handle("/", router)
	http.ListenAndServe(":8888", nil)
}

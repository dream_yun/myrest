default: build

buildTime=$(shell date -u "+%Y%m%d%I%M")
revVersion=$(shell git rev-parse --short HEAD)
#CGO_ENABLED=0
#GOOS=linux
build: 
	go build -ldflags "-X main.Version=$(revVersion).$(buildTime)"
unittest:
	@sudo service myrest stop
	@cp myrest /opt/bin/myrest
	@sudo service myrest start
	@cd test && go test divide_test.go

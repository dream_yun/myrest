package test

import "io/ioutil"
import "net/http"
import "testing"

// 일반적인 나눗셈이 잘 되는지
func TestDivide_6d2(t *testing.T) {
	resp, err := http.Get("http://localhost:8888/divide/6/2")
	if err != nil {
		t.Fatal(err.Error())
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Error("HTTP Request error ", resp.StatusCode)
	}
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Error("Divide Error 6/2 : ", err.Error())
		return
	}

	if string(result) != "3" {
		t.Error("Divide Error 6/2 = ", result)
	}
}

// 분모가 0일 경우 400 (Bad Request)를 리턴해야 한다.
func TestDivide_5d0(t *testing.T) {
	resp, err := http.Get("http://localhost:8888/divide/5/0")
	if err != nil {
		t.Fatal(err.Error())
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusBadRequest {
		t.Error("HTTP Request error ", resp.StatusCode)
	}
}
